const glob = require('glob');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const fractal = require('./fractal.config.js');

const logger = fractal.cli.console
const COMPONENT_DIR = fractal.components.get('path');
const ICON_DIR = `${COMPONENT_DIR}/01-atoms/00-icons`;
const TEMPLATE_DIR = `${COMPONENT_DIR}/04-templates`;
const ASSETS_DIR = './assets';
const SRC_DIR = './src/';
const PUB_DIR = './static';

const entry = {
  main: [
    path.resolve(SRC_DIR, 'scripts/main.js'),
    path.resolve(SRC_DIR, 'styles/tokens.css'),
    path.resolve(SRC_DIR, 'styles/main.css'),
  ].concat(glob.sync(`${ICON_DIR}/**/*.svg`)),
};

const component_assets = glob.sync(`${COMPONENT_DIR}/**/*.@(css|scss|js)`, {
  ignore: [
    `${COMPONENT_DIR}/**/*.config.js`,  // Fractal component config
    `${TEMPLATE_DIR}/**/*`,  // Template assets
  ],
});

if (component_assets.length > 0) {  // entry entries cannot be empty
  entry.components = component_assets;
}

// Handle template assets separately
glob.sync(`${TEMPLATE_DIR}/**/*.njk`).forEach((templatePath) => {
  const templateDir = path.dirname(templatePath);
  const assets = glob.sync(`${templateDir}/*.@(css|scss|js)`, {
    ignore: `${templateDir}/*.config.js`,
  });
  if (assets.length > 0) {
    const entryName = path.relative(TEMPLATE_DIR, templateDir).replace(/[0-9]+-/, '');
    const templateName = entryName.split('/').reverse()[0]
    entry[`templates/${entryName}/${templateName}`] = assets;
  }
});


module.exports = {
  entry: entry,
  output: {
    path: path.resolve(PUB_DIR),
    filename: 'scripts/[name].js',
  },
  module: {
    rules: [
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-sprite-loader',
            options: {
              extract: true,
              spriteFilename: (svgPath) => {
                const spriteName = path.relative(
                  ICON_DIR, path.dirname(path.dirname(svgPath))
                ).replace('/', '-');
                return `images/sprites/${spriteName}.svg`;
              },
              symbolId: '[name]',
              esModule: false,
            },
          },
          'svgo-loader',
        ],
      },
      {
        test: /\.scss|\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(woff(2)?)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
            },
          },
        ],
      },
      {
        test: /\.(png|jp(e*)g|gif|svg)$/,
        loader: 'url-loader',
        include: path.resolve(SRC_DIR, 'images'),
        options: {
          limit: 10000,
          name: 'images/[name].[ext]',
        },
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ['public', 'static'],
    }),
    new MiniCssExtractPlugin({
      filename: 'styles/[name].css',
      chunkFilename: 'styles/[id].css',
    }),
    new SpriteLoaderPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(SRC_DIR, '_fractal'),
          to: path.resolve(PUB_DIR, '_fractal'),
        }, // Fractal style overrides
        {
          from: path.resolve(ASSETS_DIR, 'images'),
          to: path.resolve(PUB_DIR, 'images'),
        },
        {
          from: path.resolve(ASSETS_DIR, 'meta'),
          to: path.resolve(PUB_DIR, ''),
        },
      ],
    }),
  ],
};
