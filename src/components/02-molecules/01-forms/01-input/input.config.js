module.exports = {
  title: 'Input',
  label: 'Text Inputs',
  status: 'wip',
  collated: true,
  default: 'text',
  variants: [
    {
      name: 'text',
      label: 'Text',
      context: {
        label: 'Basic Text Input',
        id: 'basicText',
      },
    },
    {
      name: 'Email',
      context: {
        label: 'Email Input',
        id: 'email',
        placeholder: 'joebloggs@example.com',
      }
    },
    {
      name: 'Password',
      context: {
        label: 'Password Input',
        id: 'password',
      }
    },
    {
      name: 'Textarea',
      context: {
        label: 'Textarea',
        id: 'textarea',
      }
    },
    {
      name: 'Date',
      context: {
        label: 'Date Input',
        id: 'date',
      }
    }
  ],
  context: {
    placeholder: 'Enter text here',
  },
};
