module.exports = {
  label: 'Sea creatures',
  prefix: 'icons-sea_creatures',
  context: {
    symbol_sprite: '/images/sprites/sea_creatures.svg',
  },
};
