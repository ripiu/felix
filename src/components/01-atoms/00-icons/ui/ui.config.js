module.exports = {
  label: 'UI',
  prefix: 'icons-ui',
  context: {
    symbol_sprite: '/images/sprites/ui.svg',
  },
};
