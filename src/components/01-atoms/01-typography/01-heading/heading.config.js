module.exports = {
  title: 'Heading',
  label: 'Headings',
  status: 'wip',
  collated: true,
  default: 'h1',
  variants: [
    {
      name: 'h1',
      label: 'H1',
    },
    {
      name: 'h2',
      label: 'H2',
    },
    {
      name: 'h3',
      label: 'H3',
    },
    {
      name: 'h4',
      label: 'H4',
    },
    {
      name: 'h5',
      label: 'H5',
    },
    {
      name: 'h6',
      label: 'H6',
    }
  ],
  context: {
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  },
};
