module.exports = {
  title: 'List',
  label: 'Lists',
  status: 'wip',
  context: {
    items: [
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      },
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      },
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      },
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      },
    ],
  },
  default: 'unordered',
  variants: [
    {
      name: 'unordered',
      label: 'Unordered',
    },
    {
      name: 'ordered',
      label: 'Ordered',
    },
    {
      name: 'description',
      label: 'Description',
      context: {
        items: [
          {
            term: 'Lorem ipsum',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          },
          {
            term: 'Lorem ipsum',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          },
          {
            term: 'Lorem ipsum',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          },
          {
            term: 'Lorem ipsum',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          },
        ],
      },
    },
  ],
};
