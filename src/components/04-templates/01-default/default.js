/**
 * Default page template
 */

const { domReady } = require('../../../scripts/utils.js');

domReady(() => {
  console.log("Hi, I'm the default page template!");
});
