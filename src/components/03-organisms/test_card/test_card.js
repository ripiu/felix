/**
 * Test card
 */

const { domReady } = require('../../../scripts/utils.js');

domReady(() => {
  document.querySelectorAll('.fx-c-card').forEach((card) => {
    const title = card.querySelector('h2').innerText;
    console.log(`Found a card: "${title}"`);
  });
});
