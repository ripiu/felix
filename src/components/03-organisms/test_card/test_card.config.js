module.exports = {
  label: 'Card',
  title: 'Test card component',
  status: 'prototype',
  context: {
    title: 'This is a title',
    picture: {
      src: '//via.placeholder.com/640x480/dddddd/ffffff.jpg',
      alt_text: 'Placeholder image',
    },
    summary: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  },
};
