const { merge } = require('webpack-merge');
const FractalWebpackPlugin = require('fractal-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map',
  output: {
    publicPath: '/'
  },
  plugins: [
    new FractalWebpackPlugin({
      configPath: './fractal.config.js',
      mode: 'server',
      sync: true,
    }),
  ],
});
